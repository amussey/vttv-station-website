<?php
include "php/common.php";


function profile_bubble($name, $position, $pid, $image_url) {
?>
        <div class="col-md-2 portfolio-item">
          <a href="mailto:<?=$pid ?>@vt.edu" class="officer-link">
            <img class="img-circle img-responsive" src="<?=$image_url ?>">
            <center class="hidden-xs hidden-sm"><b><?=$name ?></b><br /><?=$position ?></center>
          </a>
        </div>
<?php
}

?><!DOCTYPE html>
<html lang="en">
  <head>
<?php meta_data(); ?>

    <title>About<?=$title_tail; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS for the '4 Col Portfolio' Template -->
    <link href="css/4-col-portfolio.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
  </head>

  <body>

<?php include "php/nav.php"; ?>

    <!-- Page Content -->

    <div class="container">
      
      <div class="row">
      
        <div class="col-lg-12">
          <h1 class="page-header">About VTTV<!-- <small>We'd Love to Hear From You!</small>--></h1>
        </div>
        
        <div class="col-lg-12">
          <!-- Embedded Google Map using an iframe - to select your location find it on Google maps and paste the link as the iframe src. If you want to use the Google Maps API instead then have at it! -->
          <iframe width="100%" height="400px"
            frameborder="0" scrolling="no"
            marginheight="0" marginwidth="0"
            src="https://www.google.com/maps/embed?pb=!1m5!3m3!1m2!1s0x884d95733ac1cb4d%3A0xf8d9c76521fc0952!2sSquires+Student+Center%2C+Blacksburg%2C+VA+24060!5e0!3m2!1sen!2sus!4v1389728892298"></iframe>
        </div>

      </div><!-- /.row -->
      
      <div class="row">

        <div class="col-sm-8">
          <h3>Let's Get In Touch!</h3>
          <p>
            Virginia Tech TV is a student run organization based out of Squires Student Center
            on the Virginia Tech campus.  VTTV provides television programming unique to our community that would otherwise
            not be available.  We provide an opportunity for members of the college to learn all aspects of
            production and management related to the field of television and broadcast media.
          </p>
          <p>
            VTTV began in the spring semester of 1988 as a class implemented by the Communication Studies Department 
            of Virginia Tech. The class was sponsored in cooperation with Communication Network Services (CNS), 
            which provided equipment, installation, and a channel on which to broadcast. After one semester, the 
            class was not offered again. Students in the class felt the need to organize and approached Student 
            Budget Board to request funds to buy more equipment and promote the station. The students in the class 
            elected a general manager and management staff and the first general meeting was held in the fall semester 
            of 1989. VTTV approached the Student Media Board in November 1990 and was accepted as a member of the board. 
            In 1997, VTTV separated away from the University, in conjunction with the other student media organizations 
            to form the Educational Media Company at Virginia Tech (EMCVT).
          </p>
          <p>
            To this day, VTTV is broadcast 24 hours per day, 7 days per week on Channel 33 inside the Virginia Tech dorms.
            The constantly changing set of shows is also available off campus through
            <a href="http://youtube.com/virginiatechtv">YouTube</a>.
          </p>
        </div>

        <div class="col-sm-4">
          <h3>Virginia Tech Television</h3>
          <h4>Channel 33</h4>
          <p>
            340 Squires Student Center<br>
            Blacksburg, VA 24061<br>
          </p>
          <p><i class="fa fa-phone"></i> <abbr title="Phone">P</abbr>: (540) 923-0320</p>
          <p><i class="fa fa-envelope-o"></i> <abbr title="Email">E</abbr>: <a href="mailto:contact@vttv33.com">contact@vttv33.com</a></p>
          <ul class="list-unstyled list-inline list-social-icons">
            <li class="tooltip-social facebook-link"><a href="https://www.facebook.com/VTTV33" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook-square fa-2x"></i></a></li>
            <!-- <li class="tooltip-social linkedin-link"><a href="#linkedin-company-page" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin-square fa-2x"></i></a></li> -->
            <li class="tooltip-social twitter-link"><a href="https://twitter.com/vttv33" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter-square fa-2x"></i></a></li>
            <li class="tooltip-social google-plus-link"><a href="https://plus.google.com/111644249098869857667" data-toggle="tooltip" data-placement="top" title="Google+"><i class="fa fa-google-plus-square fa-2x"></i></a></li>
          </ul>
        </div>

      </div><!-- /.row -->

    </div><!-- /.container -->

    <div class="container hidden-xs hidden-sm">
      <div class="row">
        <div class="col-lg-12" style="margin-top:50px;">
          &nbsp;
        </div>
      </div>
      <?php

      $officers = read_officers();

      $officer_rows = intval(count($officers)/6);
      $officer_part_rows = count($officers) % 6;


      for ($i = 0; $i < $officer_rows; $i++) {
      ?>
        <div class="row">
        <?
        for ($j = 0; $j < 6; $j++) {
          $officer = $officers[(($i*6)+$j)];
          profile_bubble($officer["name"], $officer["position"], $officer["pid"], $officer["image_url"]);
        }
        ?>
      </div>
      <?php
      }

      if ($officer_part_rows > 0) {
      ?>
      <div class="row">
        <div class="col-md-<?=(6-$officer_part_rows) ?> portfolio-item"></div>
        <?php
        for ($i = 0; $i < $officer_part_rows; $i++) {
          $officer = $officers[(($officer_rows*6)+$i)];
          profile_bubble($officer["name"], $officer["position"], $officer["pid"], $officer["image_url"]);
        }
        ?>
        <div class="col-md-<?=(6-$officer_part_rows) ?> portfolio-item"></div>
      </div>
      <?php
      }
      ?>
      
      
    </div><!-- /.container -->


<?php include "php/copyright.php" ?>
<?php include "php/common.js.php" ?>
    <script src="js/modern-business.js"></script>

  </body>
</html>
