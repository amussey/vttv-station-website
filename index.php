<?php
include "php/common.php";

?><!DOCTYPE html>
<html lang="en">
  <head>
<?php meta_data(); ?>

    <title>Virginia Tech Television | VTTV33 | A Division of EMCVT</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="css/half-slider.css" rel="stylesheet">
    <link href="css/vttv.css" rel="stylesheet">

  </head>

  <body>

<?php include "php/nav.php" ?>

    <!-- Header -->
    <header>
      <div class="intro-header">
        <div class="container"></div>
      </div>
    </header>

    <div class="container">

      <div class="row section">
        <div class="col-lg-8">
          <h1>Virginia Tech's Premiere Television Station</h1>
          <p>
            Virginia Tech TV (VTTV) is the university's student run television station.  For over 30 years, the station has been covering the latest in sports, entertainment, and humor in the Blacksburg and greater New River Valley area.
          </p>
          <p>
            The station currently broadcasts over the campus cable network on channel 33.  The recorded shows are available online via YouTube on <a href="http://youtube.com/virginiatechtv">Virginia Tech TV's official channel</a>.
          </p>
          <p>
            Want to get involved with VTTV?  <a href="join">Find out more now</a>!
          </p>
        </div>
        <div class="col-lg-4 hidden-md hidden-xs hidden-sm">
          <!-- Begin Ad Placement -->
          <center><iframe src="img/ad-placeholder-medium-rectangle-300x250.jpg" width="300" height="250" frameBorder="0"></iframe></center>
          <!-- End Ad Placement -->
        </div>
      </div>

      <div class="row section hidden-lg hidden-xs">
        <div class="col-md-12 col-sm-12">
          <!-- Begin Ad Placement -->
          <center><iframe src="img/ad-placeholder-leaderboard-728x90.jpg" width="728" height="90" frameBorder="0"></iframe></center>
          <!-- End Ad Placement -->
        </div>
      </div>

      <?php
      $youtube_videos = @file_get_contents("https://gdata.youtube.com/feeds/api/users/virginiatechtv/uploads?alt=json");

      if ($youtube_videos) {
        $youtube_json = json_decode($youtube_videos, true);

      ?>

      <div class="row section">
        <div class="col-lg-8">
          <h2 style="margin-bottom: 20px;">Our Latest Shows</h2>
          <?php

          $total_shows = 6;
          $final_line = $total_shows % 3;
          for ($i = 0; $i < $total_shows; $i++) {
            $current_video = substr($youtube_json["feed"]["entry"][$i]["id"]["\$t"], -11);
            $video_title = $youtube_json["feed"]["entry"][$i]["title"]["\$t"];
          ?>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <center><a href="http://youtu.be/<?=$current_video ?>"><img src="https://i.ytimg.com/vi/<?=$current_video ?>/mqdefault.jpg" class="vttv-video-box"></a></center>
          </div>
          <?php } ?>
        </div>
        <div class="col-lg-4 hidden-md hidden-xs hidden-sm">
          <!-- Begin Ad Placement -->
          <center><iframe src="img/ad-placeholder-medium-rectangle-300x250.jpg" width="300" height="250" frameBorder="0"></iframe></center>
          <!-- End Ad Placement -->
        </div>
      </div>
      <?php
      } 
      ?>
      

      <div class="row section hidden-xs">
        <div class="col-lg-12 col-md-12 col-sm-12 ">
          <!-- Begin Ad Placement -->
          <center><iframe src="img/ad-placeholder-leaderboard-728x90.jpg" width="728" height="90" frameBorder="0"></iframe></center>
          <!-- End Ad Placement -->
        </div>
      </div>
    </div><!-- /.container -->

<?php include "php/copyright.php" ?>
<?php include "php/common.js.php" ?>
  </body>
</html>
