<?php
include "php/common.php";

?><!DOCTYPE html>
<html lang="en">
  <head>
<?php meta_data(); ?>

    <title>Shows | VTTV33 | A Division of EMCVT</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS for the '4 Col Portfolio' Template -->
    <link href="css/4-col-portfolio.css" rel="stylesheet">
</head>

<body>

<?php include "php/nav.php" ?>

    <div class="container">
        <?php

        $shows = read_shows();
        $active_shows = $shows["current"]; 
        $inactive_shows = $shows["past"];

        ?>
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Current Shows
                    <small>What we're airing right now</small>
                </h1>
            </div>

        </div>

        <?php

        for ($i = 0; $i < count($active_shows); $i++) {
            if ($i%4 == 0) {
                ?>
        <div class="row"><?php
            }
            ?>
            <div class="col-md-3 portfolio-item">
                <a href="<?=$active_shows[$i]["url"] ?>">
                    <img class="img-responsive" src="<?=$active_shows[$i]["image_url"] ?>">
                </a>
            </div>
            <?php
            if (($i == count($active_shows)-1) || (($i+1)%4 == 0)) {
                ?>
        </div><?php
            }
        }
        ?>

        <hr>

        <div class="row section">
            <div class="col-lg-12 hidden-md hidden-xs hidden-sm">
                <!-- Begin Ad Placement -->
                <center><iframe src="img/ad-placeholder-leaderboard-728x90.jpg" width="728" height="90" frameBorder="0"></iframe></center>
                <!-- End Ad Placement -->
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Past Shows
                    <small>Where we've been</small>
                </h1>
            </div>

        </div>

        <?php

        for ($i = 0; $i < count($inactive_shows); $i++) {
            if ($i%4 == 0) {
                ?>
        <div class="row"><?php
            }
            ?>
            <div class="col-md-3 portfolio-item">
                <a href="<?=$inactive_shows[$i]["url"] ?>">
                    <img class="img-responsive" src="<?=$inactive_shows[$i]["image_url"] ?>">
                </a>
            </div>
            <?php
            if (($i == count($inactive_shows)-1) || (($i+1)%4 == 0)) {
                ?>
        </div><?php
            }
        }
        ?>

    </div>
    <!-- /.container -->

<?php include "php/copyright.php" ?>
<?php include "php/common.js.php" ?>

</body>

</html>
