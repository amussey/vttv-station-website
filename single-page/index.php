<?php
$youtube_videos = @file_get_contents("https://gdata.youtube.com/feeds/api/users/virginiatechtv/uploads?alt=json");



?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta property="og:image" content="img/bw_logo.png" />
    <link rel="shortcut icon" href="img/favicon_32x32.ico" />

    <title>Virginia Tech Television | VTTV33 | A Division of EMCVT</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="vttv.css" rel="stylesheet">

  </head>
  <body>

    <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="." style="color:#fff;">
            <img src="img/tvlogo_white_500.png" width="30" style="margin-right:5px; margin-top:-4px;"> VTTV
          </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav">
            <li><a href="http://youtube.com/virginiatechtv" target="_blank">YouTube</a></li>
            <li><a href="http://facebook.com/vttv33">Facebook</a></li>
            <li><a href="http://twitter.com/vttv33">Twitter</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>


    <div class="container">
      <div class="row section">
        <div class="col-lg-12">
          <h1>Virginia Tech's Premiere Television Station</h1>
          <p>
            Virginia Tech TV is a student run organization based out of Squires Student Center
            on the Virginia Tech campus.  VTTV provides television programming unique to our community that would otherwise
            not be available.  We provide an opportunity for members of the college to learn all aspects of
            production and management related to the field of television and broadcast media.
          </p>
          <p>
            VTTV began in the spring semester of 1988 as a class implemented by the Communication Studies Department 
            of Virginia Tech. The class was sponsored in cooperation with Communication Network Services (CNS), 
            which provided equipment, installation, and a channel on which to broadcast. After one semester, the 
            class was not offered again. Students in the class felt the need to organize and approached Student 
            Budget Board to request funds to buy more equipment and promote the station. The students in the class 
            elected a general manager and management staff and the first general meeting was held in the fall semester 
            of 1989. VTTV approached the Student Media Board in November 1990 and was accepted as a member of the board. 
            In 1997, VTTV separated away from the University, in conjunction with the other student media organizations 
            to form the Educational Media Company at Virginia Tech (EMCVT).
          </p>
          <p>
            To this day, VTTV is broadcast 24 hours per day, 7 days per week on Channel 33 inside the Virginia Tech dorms.
            The constantly changing set of shows is also available off campus through
            <a href="http://youtube.com/virginiatechtv">YouTube</a>.
          </p>
        </div>
      </div>



      <?php
      if ($youtube_videos) {
        $youtube_json = json_decode($youtube_videos, true);
      ?>
      <div class="section">
        <div class="row">
          <div class="col-lg-12">
            <h2 style="margin-bottom: 20px;">Our Latest Shows</h2>
          </div>
        </div>
        <div class="row">
          <?php

          $total_shows = 8;
          for ($i = 0; $i < $total_shows; $i++) {
            $current_video = substr($youtube_json["feed"]["entry"][$i]["id"]["\$t"], -11);
            $video_title = $youtube_json["feed"]["entry"][$i]["title"]["\$t"];
          ?>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <center><a href="http://youtu.be/<?=$current_video ?>"><img src="https://i.ytimg.com/vi/<?=$current_video ?>/mqdefault.jpg" class="vttv-video-box"></a></center>
          </div>
          <?php } ?>
        </div>
      </div>
      <?php
      } 
      ?>


    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <h2>Join Our Team</h2>
          <p>
            Virginia Tech TV is a completely student run organization. 
            All of our shows are written, produced, and edited by members of the student body.
            If you've ever had an interest in producing the content you see every day on TV,
            we need your help to keep our station running.  Have an improvement you want to see
            in a show?  Want to start your OWN show?  We're always open to ideas, recommendations,
            and newcomers to bring fresh ideas and views to the table.
          </p>
          <p>
            Send us <a href="mailto:contact@vttv33.com">an email</a> or <a href="https://www.facebook.com/VTTV33" target="_blank">visit our Facebook page</a> to find out all about when our next meeting is and to join our listserv!
          </p>
        </div>
        
        <div class="col-sm-4">
          <h3>Visit Our Studio</h3>
          <h4>VTTV, Channel 33</h4>
          <p>
            340 Squires Student Center<br>
            Blacksburg, VA 24061<br>
          </p>
          <p><i class="fa fa-phone"></i> <abbr title="Phone">P</abbr>: (540) 923-0320</p>
          <p><i class="fa fa-envelope-o"></i> <abbr title="Email">E</abbr>: <a href="mailto:contact@vttv33.com">contact@vttv33.com</a></p>
          <ul class="list-unstyled list-inline list-social-icons">
            <li class="tooltip-social youtube-link"><a href="http://youtube.com/virginiatechtv" data-toggle="tooltip" data-placement="top" title="YouTube"><i class="fa fa-youtube-square fa-2x"></i></a></li>
            <li class="tooltip-social facebook-link"><a href="https://www.facebook.com/VTTV33" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook-square fa-2x"></i></a></li>
            <li class="tooltip-social twitter-link"><a href="https://twitter.com/vttv33" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter-square fa-2x"></i></a></li>
            <li class="tooltip-social google-plus-link"><a href="https://plus.google.com/111644249098869857667" data-toggle="tooltip" data-placement="top" title="Google+"><i class="fa fa-google-plus-square fa-2x"></i></a></li>
          </ul>
        </div>
      </div><!-- /.row -->
    </div><!-- /.container -->


    <div class="container">
        <hr>
        <footer>
            <div class="row">
                <div class="col-lg-12" style="color:#919191;">
                    <p>&copy; <?=date("Y") ?> VTTV &amp; <a href="http://www.collegemedia.com/">Educational Media Company at Virginia Tech, Inc.</a> All rights reserved.<br />
                    Designed by <a href="http://amussey.com">Andrew Mussey</a>.</p>
                </div>
            </div>
        </footer>
    </div>
    <!-- /.container -->


    <!-- JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  </body>
</html>
