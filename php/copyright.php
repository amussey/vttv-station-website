    <div class="container">

        <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12" style="color:#919191;">
                    <p>&copy; <?=date("Y") ?> VTTV &amp; <a href="http://www.collegemedia.com/">Educational Media Company at Virginia Tech, Inc.</a> All rights reserved.<br />
                    Designed by <a href="http://amussey.com">Andrew Mussey</a>.</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->
