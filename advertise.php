<?php
include "php/common.php";


?><!DOCTYPE html>
<html lang="en">
  <head>
<?php meta_data(); ?>

    <title>Advertise or Sponsor | VTTV33 | A Division of EMCVT</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="css/modern-business.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
  </head>

  <body>

<?php include "php/nav.php" ?>

    <div class="container" style="margin-bottom:40px;">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Advertise or Sponsor
                    <small></small>
                </h1>
            </div>

        </div>

        <div class="row">

            <div class="col-md-6">
                <iframe width="100%" height="300" src="//www.youtube.com/embed/MXWG2W5AQFY" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
                <h2>Let Us Get Your Name Out</h2>
                <p>
                    Like all great TV stations, we can't continue to put together our shows without your support.
                    Virginia Tech TV is a great place to tell the greater Blacksburg community (or even the world)
                    about your business.
                </p>
                <p>
                    We're happy to produce commercials or videos for your cause, business, or organization.  Our
                    highly trained team has had hundreds of hours of experience behind the camera and thousands
                    of hours of experience editing.  Let us help you put your best foot forward. 
                </p> 
            </div>

        </div>

    </div>

    <div class="section-colored">

        <div class="container">

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h2>Thousands on TV, Millions online</h2>
                    <p>
                        Virginia Tech TV is currently broadcasting live on-campus to VT's nearly 30,000 students
                        as well as off campus to many local businesses in Downtown Blacksburg.  It's broadcast internationally
                        through YouTube to the site's more than 1 billion unique visitors per month.  In the past semester,
                        over 120 videos were uploaded with thousands of unique views from around the globe.
                    </p>
                    <p>
                        When you advertise with Virginia Tech TV, your ads can get placed in both our online and broadcast
                        shows.  You can select specific shows, or our entire catalog, so you can be sure to hit your target
                        market. 
                    </p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
                    <img class="img-responsive" src="img/advertise-globe.png">
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

    </div>
    <!-- /.section-colored -->

    <div class="section">

        <div class="container">

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
                    <img class="img-responsive" src="img/advertise-onset.jpg">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h2>Sponsor our productions</h2>
                    <p>
                        We're producing shows seven days per week, and can always use:
                    </p>
                    <ul>
                        <li>Camera Equipment</li>
                        <li>Lighting and set props</li>
                        <li>Computers and Editing Workstations</li>
                        <li>On-set food and beverages</li>
                        <li>Web hosting</li>
                    </ul>
                    <p>
                        Contact our management team if you have any of these resources to donate,
                        and we'd be happy to work out advertising or promotional agreements.
                    </p>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

    </div>
    <!-- /.section -->

    <div class="section-colored">
        <div class="container">
            <div class="row text-center" style="margin-bottom:40px;">
                <h1>Contact our team now!</h1>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <button onclick="location.href='mailto:contact@vttv33.com?subject=Interest in running a commercial'" class="btn btn-primary btn-lg" style="width:100%; height:70px;">
                        Get your commercial on TV!
                    </button>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <button onclick="location.href='mailto:contact@vttv33.com?subject=Estimate on having a video produced'" class="btn btn-primary btn-lg" style="width:100%; height:70px;">
                        Get an estimate on video production!
                    </button>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <button onclick="location.href='mailto:contact@vttv33.com?subject=Sponsorship Interest'" class="btn btn-primary btn-lg" style="width:100%; height:70px;">
                        Contact us about a sponsorship!
                    </button>
                </div>
            </div>
        </div>

    </div><!-- /.section -->


    <div class="container">
        <!-- Our Customers -->

        <div class="row">

            <div class="col-lg-12">
                <h2 class="page-header">Past and Present Sponsors</h2>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6">
                <a href="http://www.directsattv.com/directv">
                    <img class="img-responsive img-customer" src="img/sponsors/directtv.png">
                </a>
            </div>

<!--             <div class="col-md-2 col-sm-4 col-xs-6">
                <img class="img-responsive img-customer" src="http://placehold.it/500x300">
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6">
                <img class="img-responsive img-customer" src="http://placehold.it/500x300">
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6">
                <img class="img-responsive img-customer" src="http://placehold.it/500x300">
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6">
                <img class="img-responsive img-customer" src="http://placehold.it/500x300">
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6">
                <img class="img-responsive img-customer" src="http://placehold.it/500x300">
            </div> -->

        </div>

    </div>
    <!-- /.container -->

<?php include "php/copyright.php" ?>
<?php include "php/common.js.php" ?>
    <script src="js/modern-business.js"></script>

</body>

</html>
