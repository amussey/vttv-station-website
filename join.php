<?php
include "php/common.php";


?><!DOCTYPE html>
<html lang="en">
  <head>
<?php meta_data(); ?>

    <title>Join the Team | VTTV33 | A Division of EMCVT</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS for the '4 Col Portfolio' Template -->
    <link href="css/4-col-portfolio.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
  </head>

  <body>

<?php include "php/nav.php"; ?>

    <!-- Page Content -->

    <div class="container">
      
      <div class="row">
      
        
        
        <div class="col-lg-12">
          <img src="img/vttv-team.jpg" width="100%">
        </div>

      </div><!-- /.row -->

      <div class="row">

        <div class="col-sm-8">
          <h2>Join Our Team</h2>
          <p>
            Virginia Tech TV is a completely student run organization. 
            All of our shows are written, produced, and edited by members of the student body.
            If you've ever had an interest in producing the content you see every day on TV,
            we need your help to keep our station running.  Have an improvement you want to see
            in a show?  Want to start your OWN show?  We're always open to ideas, recommendations,
            and newcomers to bring fresh ideas and views to the table.
          </p>
          <p>
            Send us <a href="mailto:contact@vttv33.com">an email</a> or <a href="https://www.facebook.com/VTTV33" target="_blank">visit our Facebook page</a> to find out all about when our next meeting is and to join our listserv!
          </p>

          <div class="col-lg-12 hidden-md hidden-xs hidden-sm" style="margin-top:40px;">
            <!-- Begin Ad Placement -->
            <center><iframe src="img/ad-placeholder-leaderboard-728x90.jpg" width="728" height="90" frameBorder="0"></iframe></center>
            <!-- End Ad Placement -->
          </div>
        </div>

        <div class="col-sm-4">
          <h3>Come to Our Next Meeting!</h3>
          <p>
            Interested in helping out?  We post the times and locations of our interest meetings on our <a href="https://www.facebook.com/VTTV33" target="_blank">Facebook page</a>.
          </p>
          <p>
            Have a particular show you want to be a part of?  Send us <a href="mailto:contact@vttv33.com">an email</a> or a message to our <a href="https://www.facebook.com/VTTV33" target="_blank">Facebook page</a> and we can direct you to the right place!
          </p>

          <h3>Visit Our Studio</h3>
          <p>
            340 Squires Student Center<br>
            Blacksburg, VA 24061<br>
          </p>
        </div>

      </div><!-- /.row -->

    </div><!-- /.container -->

<?php include "php/copyright.php" ?>
<?php include "php/common.js.php" ?>
    <script src="js/modern-business.js"></script>

  </body>
</html>
