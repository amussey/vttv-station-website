<?php

$title_tail = " | VTTV33 | A Division of EMCVT";
$officer_file = "admin/config/officers.cfg";
$show_file = "admin/config/shows.cfg";


////////////// END OF EDITABLE SECTION //////////////

// Define the officer structure
$officer_structure = array("name", "position", "pid", "image_url");

// Define the show structure
$show_structure = array("active", "title", "image_url", "url");


function meta_data() {
?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta property="og:image" content="http://amussey.com/projects/vttv/img/bw_logo.png" />
    <link rel="shortcut icon" href="img/favicon_32x32.ico" />

<?php
}


/**
 * Parse the file with the list of officers and return them as an array.
 */
function read_officers($cfg_location="") {
    global $officer_file, $officer_structure;
    $file_contents = file_get_contents($cfg_location.$officer_file);
    $officer_list = explode("\n", $file_contents);
    $officers = array();
    foreach ($officer_list as &$officer) {
        if ($officer == "") break;
        $officer_exploded = explode("    ", $officer);
        $officer = array();
        for ($i = 0; $i < count($officer_structure); $i++) {
            $officer[$officer_structure[$i]] = $officer_exploded[$i];
        }
        array_push($officers, $officer);
    }
    return $officers;
}


/**
 * Parse the list of shows and return them as an array.
 */
function read_shows($cfg_location="") {
    global $show_file, $show_structure;
    $file_contents = file_get_contents($cfg_location.$show_file);
    $show_list = explode("\n", $file_contents);
    $shows = array(
        "current" => array(),
        "past" => array()
    );
    foreach ($show_list as &$show) {
        if ($show == "") break;
        $show_exploded = explode("    ", $show);
        $show = array();
        for ($i = 0; $i < count($show_structure); $i++) {
            $show[$show_structure[$i]] = $show_exploded[$i];
        }
        if ($show["active"] == "0") {
            array_push($shows['past'], $show);
        } else {
            array_push($shows['current'], $show);
        }
    }

    return $shows;
}


function blank_officer() {
    global $officer_structure;
    $officer = array();
    foreach($officer_structure as &$descriptor) {
        $officer[$descriptor] = "";
    }
    return $officer;
}

function blank_show() {
    global $show_structure;
    $show = array();
    foreach($show_structure as &$descriptor) {
        if ($descriptor == "active") {
            $show[$descriptor] = 1;
        } else {
            $show[$descriptor] = "";
        }
    }
    return $show;
}
