<?php
require_once("../php/common.php");

if (isset($_POST["submit"])) {
    $header = "../";
    // write the officers
    $officer_txt = "";
    for ($i = 0; $i < count($_POST["officer_".$officer_structure[1]]); $i++) {
        if ($_POST["officer_".$officer_structure[1]][$i] == "") {
            break;
        }
        $officer_row = "";
        for ($j = 0; $j < count($officer_structure); $j++) {
            $officer_row .= $_POST["officer_".$officer_structure[$j]][$i].(($j+1 == count($officer_structure)) ? "\n" : "    ");
        }
        $officer_txt .= $officer_row;
    }
    file_put_contents($header.$officer_file, $officer_txt);

    // write the shows
    $show_txt = "";
    for ($i = 0; $i < count($_POST["show_".$show_structure[1]]); $i++) {
        if ($_POST["show_".$show_structure[1]][$i] == "") {
            break;
        }
        $show_row = "";
        for ($j = 0; $j < count($show_structure); $j++) {
            $show_row .= $_POST["show_".$show_structure[$j]][$i].(($j+1 == count($show_structure)) ? "\n" : "    ");
        }
        $show_txt .= $show_row;
    }
    file_put_contents($header.$show_file, $show_txt);
}


?>
<html>
<head>
    <title>Admin Panel</title>
</head>
<body>
    <h1>Admin Panel</h1>
    <a href="password.php">Click here</a> to change the password to the admin panel.
    <hr>
    <form method="POST" action="">
    <h3>Officers</h3>

    You must upload the officer's image via FTP to the <code>img/staff/</code> directory on the web server.
    The resolution MUST be <b>250x250</b>.<br>
    <br>

    <table border="1">
        <tr>
        <?php
        $officers = read_officers("../");
        foreach($officer_structure as &$descriptor) {
            ?><th><?=$descriptor ?></th><?php
        }
        ?>
        <tr>
        <?php
        for ($i = 0; $i < 20; $i++) {
            ?><tr><?php
            if (isset($officers[$i])) {
                $officer = $officers[$i];
            } else {
                $officer = blank_officer();
            }
            for ($j = 0; $j < count($officer_structure); $j++) {
                ?><td><input type="text" value="<?=$officer[$officer_structure[$j]] ?>" style="width:200px;" name="officer_<?=$officer_structure[$j] ?>[]"></td><?php
            }
            ?></tr><?php
        }
        ?>
    </table>
    <br><br><br>
    <hr>
    <h3>Shows</h3>
    You must upload the show image via FTP to the <code>img/shows/</code> directory on the web server.
    The recommended resolution is <b>750x450</b> or higher.<br>
    <img src="../img/shows/TechTonight.jpg" width="300"><br>
    <br>

    <table border="1">
        <tr>
        <?php
        $shows = read_shows("../");
        foreach($show_structure as &$descriptor) {
            ?><th><?=$descriptor ?></th><?php
        }
        ?>
        <tr>
        <?php
        for ($i = 0; $i < 20; $i++) {
            if (isset($shows["current"][$i])) {
                $show = $shows["current"][$i];
            } else if (isset($shows["past"][$i-count($shows["current"])])) {
                $show = $shows["past"][$i-count($shows["current"])];
            } else {
                $show = blank_show();
            }
            ?><tr><?php
            for ($j = 0; $j < count($show_structure); $j++) {
                ?><td><?php
                if ($show_structure[$j] == "active") {
                    ?>
                    <select name="show_active[]">
                        <option value="1">Current</option>
                        <option value="0" <?=($show["active"] == 0 ? "selected" : "") ?>>Past</option>
                    </select>
                    <?php
                } else {
                    ?><input type="text" value="<?=$show[$show_structure[$j]] ?>"  style="width:200px;" name="show_<?=$show_structure[$j] ?>[]"><?php
                }
                ?></td><?php
            }
            ?></tr><?php
        }
        ?>
    </table>
    <br><br><br>
    <hr>
    <input type="submit" value="Save" style="font-size:200px;" name="submit">

    </form>
</body>
</html>
