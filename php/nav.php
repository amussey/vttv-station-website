    <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="." style="color:#fff;">
            <img src="img/tvlogo_white_500.png" width="30" style="margin-right:5px; margin-top:-4px;"> VTTV
          </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav">
            <li><a href="http://youtube.com/virginiatechtv" target="_blank">Watch</a></li>
            <li><a href="shows">Shows</a></li>
            <li><a href="advertise">Advertise</a></li>
            <li><a href="about">About</a></li>
            <li><a href="join">Join</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
